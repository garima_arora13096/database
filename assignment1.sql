Create Table Customer (
CustomerId Int Not Null Identity(1,1),
Name Varchar(20),
Customer_no Char(10),
Address Varchar(20),
City Varchar(20),
State Varchar(20),
Country Varchar(20),
PostalCode Varchar(20),
Primary Key(CustomerId)
);

Create Table ProductDetails (
ProductCode Int Not Null,
ProductName Varchar(20),
Quantity Int,
Amount Int,
Primary Key(ProductCode),
);

Create Table OrderDetails (
OrderId Int Not Null Identity(1,1),
Order_Date DateTime Default getdate(),
Shipped_Date DateTime,
Status Varchar(10),
Primary Key(OrderId),
);


Create Table Payment (
Customer_no Char(10) ,
Price Int,
CustomerId Int Foreign Key references Customer(CustomerId) 
);

Create Table OrderPlace (
OrderNo Int Not Null ,
ProductCode Int,
Quantity Int,
Price Int
Primary Key(OrderNo),
Foreign Key(ProductCode) References ProductDetails(ProductCode)
);

--insert
Insert Into Customer Values 
( 'garima' , '6474848494' , '12-A' , 'Udaipur' , 'Raj' , 'India'  , '313001' ),
( 'divya' , '7484906756' , '14-A' , 'jaipur' , 'Raj' , 'India'  , '302001' ),
( 'raj' , '2345678912' , '15-A' , 'ganganagar' , 'Raj' , 'India'  , '313005' ),
( 'rohan' , '6474848494' , '17-C' , 'Udaipur' , 'Raj' , 'India'  , '313001' ),
( 'sunil' , '6454672344' , '12-D' , 'Florida' , 'US' , 'India'  , '3101' ),
( 'luv' , '6474873834' , '12-A' , 'Udaipur' , 'Raj' , 'India'  , '313001' ),
( 'Kumar' , '9085674555' , '14-B' , 'ganganagar' , 'Raj' , 'India'  , '313005' ),
( 'raksha' , '9876456789' , '17-C' , 'Florida' , 'US' , 'India'  , '3101' ),
( 'shlok' , '9067856745' , '12-A' , 'ganganagar' , 'Raj' , 'India'  , '313005' ),
( 'sam' , '9567845636' , '17-C' , 'Florida' , 'US' , 'India'  , '3101' );

Insert Into  OrderDetails Values 
( '2018-2-11', '2018-3-11' , 'clear' , '6474848494' , 1),
( '2017-11-10' , '2018-12-1' , 'Pending' , '7484906756'  , 2),
( '2018-3-11' , '2018-5-23' , 'Pending' , '6474873834' , 4),
( '2017-12-12' , '2018-1-11' , 'clear' , '2345678912' , 7),
( '2018-1-12' , '2018-2-11' , 'Pending' , '9085674555' , 5),
( '2017-2-23' , '2017-3-11' , 'clear' , '9876456789' , 10 ),
( '2018-4-11' , '2018-7-11' , 'clear' , '9067856745' , 9 ),
( '2018-5-12' , '2018-7-11' , 'clear' ,'6454672344',8),
( '2017-12-11' , '2018-2-11' , 'clear' , '2345678912' , 6 ),
( '2018-7-23' , '2018-9-11' , 'clear', '9567845636' , 3);

Insert Into ProductDetails Values 
(1001,'samsung',2,12000),
(1002,'Mi',1,8000),
(1003,'Iphone',3,60000),
(1004,'Mi',1,8000),
(1005,'samsung',2,15000),
(1006,'Mi',1,8000),
(1007,'samsung',1,15000),
(1008,'iphone',2,60000),
(1009,'Mi',1,8000),
(1010,'iphone',2,60000);
select * from ProductDetails;

Insert Into Payment Values
('9567845636',60000),
('7484906756',15000),
('6474848494',8000),
('2345678912',60000),
('6474848494',8000),
('6474873834',15000),
('9085674555',60000),
('9876456789',8000),
('9567845636',60000);

Insert Into OrderPlace Values
(1,1001,2,12000),
(2,1005,2,15000),
(3,1002,1,8000),
(4,1004,1,8000),
(5,1003,3,60000),
(6,1006,1,8000),
(7,1010,2,60000),
(8,1007,1,15000),
(9,1009,1,8000),
(10,1008,2,60000);
 
--Synonyms
Select CustomerId 
 As C 
  From Customer ;

--order by 
Select CustomerId , Name ,   Customer_no
 From Customer 
   Order By CustomerId,Name;

--Joins

--1.Inner
Select C.Name , C.CustomerId , O.OrderId  , O.Status
 From Customer C
  Inner Join OrderDetails O 
   On C.CustomerId = O.CustomerId ;

--2.Left
Select C.Name , C.CustomerId , O.OrderId  , O.Status
 From Customer C
  Left Join OrderDetails O 
   On C.CustomerId = O.CustomerId ;

--3.right
Select C.Name , C.CustomerId , O.OrderId  , O.Status
 From Customer C
  Right Join OrderDetails O 
   On C.CustomerId = O.CustomerId ;

--4.Full
Select C.Name , C.CustomerId , O.OrderId  , O.Status
 From Customer C
  Full Join OrderDetails O 
   On C.CustomerId = O.CustomerId ;

--5.Cross
Select C.Name , C.CustomerId , O.OrderId  , O.Status
 From Customer C
  Cross Join OrderDetails O ;

--views are Virtual tables like sql query
   Create View CustomerView1 As 
    Select CustomerId , Name Email
	 From Customer
	   Where Name='rohan' ;
Select * From CustomerView1;

 --index
Create index ind
 On OrderDetails (Order_Date ASC);
sp_helpindex OrderDetails; -- show

--group by
Select ProductName , Count(Quantity) as C
 From ProductDetails 
  Group By ProductName;

--Having clause
Select ProductName , AVG(Price)
 From ProductDetails 
  Group By ProductName
  Having AVG(Price)>5000;

--window function
Select ProductName , Price , SUM(Price) 
 Over (Order By Price) As sum From ProductDetails;


--subquery/corelated query
--subquery
Select  CustomerId , Name , Customer_no ,Address , City , State , Country , PostalCode
 From Customer 
  Where CustomerId In 
   ( Select CustomerId From Customer Where City = 'Udaipur');

 